TC2 Extractor
=============

`TC2 Extractor` aims to create a database with all the information contained in the TC2 model that are created by spanish companies to declare their workers' contributions to the state coffers.

The TC2 model is a PDF file provided by the Spanish Employment Ministry. More information: https://www.boe.es/boe/dias/1985/02/01/pdfs/A02638-02666.pdf

Usage
-----

```
usage: tc2extractor [-h] path filename

TC2 Extractor - Extract information form TC2 models and saves it in a sqlite3 database

positional arguments:
 -path: where to find TC2 models
 -filename: target database

optional arguments:
	-h, --help	shows this help message and exit
```

Dependencies
------------

* [PyOCR](https://github.com/jflesch/pyocr)
* [tesseract-ocr](https://github.com/tesseract-ocr) (sh) and the spanish language files (libtesseract won't work).
* [Wand](https://github.com/dahlia/wand.git)

TODO
----

`TC2 Extractor` is a work in progress, so any ideas and patches are appreciated.

- [X] Avoid processing already processed files.
    - [X] Create database table to store processed files.
- [ ] Import TC2 data
    - [X] From text files
    - [ ] From PDF files
        - [ ] Recover from most OCR common mistakes
- [X] Insert TC2 data to SQLite database
    - [X] Create SQLite database if it does not exist
    - [X] Append data to existing employees
    - [X] Update processed files.
- [ ] Use module level loggers
- [ ] Documentation

LICENSE
-------

`TC2 Extractor` is licensed under [GPL3](https://gitlab.com/elpasmo/tc2extractor/blob/master/COPYING)

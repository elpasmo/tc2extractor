"""
TC2 Extractor pdf import module.

Based on the tutorial:
https://pythontips.com/2016/02/25/ocr-on-pdf-files-using-python/

COPYRIGHT:
Copyright 2016 Javier Moreno López

This file is part of TC2Extractor.

TC2Extractor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TC2Extractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TC2Extractor.  If not, see <http://www.gnu.org/licenses/>.
"""

import io
import logging

from PIL import Image as PI

import pyocr
import pyocr.builders

from wand.image import Image

from data import Employee, Payment

class PageOCR:
    def __init__(self, filename, page=None, img=None):
        self.__filename = filename
        self.page = page
        self.img = img
        self.__tool = pyocr.tesseract
        if not self.__tool.is_available():
            raise ImportError('Tesseract (sh) is not present in the system.')
        self.__lang = 'spa'
        self.__textbuilder = pyocr.builders.TextBuilder()
        self.__digitbuilder = pyocr.tesseract.DigitBuilder()

    def crop_info(image):
        """
        1. Removes all non-black pixels (R>50 or G>50 or B>50).
        2. Transform near-black pixels to black (R<50 and G<50 and B<50).
        3. Returns the box that has all black pixels.
           Returns None if there is no black.

           Box has a blank margin of 5 pixels if inside image boundaries.
        """
        pixels = image.load()
        upper_left = list(image.size)
        lower_right = [0, 0]
        found = False
        for y in range(image.size[1]):
            for x in range(image.size[0]):
                if (pixels[x, y][0] > 50 or
                        pixels[x, y][1] > 50 or
                        pixels[x, y][2] > 50):
                    pixels[x, y] = (255, 255, 255)
                else:
                    pixels[x, y] = (0, 0, 0)
                    found = True
                    upper_left[0] = min((x, upper_left[0]))
                    upper_left[1] = min((y, upper_left[1]))
                    lower_right[0] = max((x, lower_right[0]))
                    lower_right[1] = max((y, lower_right[1]))

        if found:
            # Leave a blank margin
            upper_left[0] = max((0, upper_left[0] - 5))
            upper_left[1] = max((0, upper_left[1] - 5))
            lower_right[0] = min((image.size[0], lower_right[0] + 5))
            lower_right[1] = min((image.size[1], lower_right[1] + 5))

            return image.crop((upper_left[0], upper_left[1],
                              lower_right[0], lower_right[1]))
        else:
            return None

    def get_builder(self, tesseract_layout, text):
        """
        PyOCR builders have tesseract_configs as a class variable so any change
        of builders will append different tesseract parameters instead of
        removing the old ones. This method aims to give a clean builder with
        only the needed tesseract parameters set.
        """
        builder = None
        if text:
            builder = self.__textbuilder
            builder.tesseract_configs = ["-psm", str(tesseract_layout)]
        else:
            builder = self.__digitbuilder
            builder.tesseract_configs = [
                                            '-psm',
                                            str(tesseract_layout),
                                            'digits'
                                        ]

        builder.tesseract_layout = tesseract_layout

        return builder


    def scan(self, box, tesseract_layout=3, text=True):
        """
        OCR at the content of an image. If black pixels are detected but no
        text is recognized a warning is thrown and the image is saved.
        """
        image = PageOCR.crop_info(self.img.crop(box))
        if image is None:
            return ''
        else:
            txt = self.__tool.image_to_string(
                image,
                lang=self.__lang,
                builder=self.get_builder(tesseract_layout, text)
            )
            # Sometimes spurious black pixels are detected at the edges.
            if (len(txt.strip()) == 0 and image.size[0] > 14
                    and image.size[1] > 14):
                if tesseract_layout != 10:
                    builder = self.get_builder(10, text)
                    txt = self.__tool.image_to_string(
                        image,
                        lang=self.__lang,
                        builder=builder
                    )

                    if len(txt.strip()) > 0:
                        return txt

                log_image = '{0}_{1}_{2}_{3}.jpg'.format(self.__filename,
                                                         self.page,
                                                         box[0], box[1])
                print(builder.tesseract_configs)
                logging.warning('No OCR at non-empty cell. Saved: %s.',
                                log_image)
                image.save(log_image)

            return txt


def importTC2s(employees, filename):
    """
    Import TC2 information from a PDF file.

    Throws ImportError if Tesseract (sh) is not present.
    """
    location = filename[:3]
    year = filename[3:7]
    name = filename[:-4]
    pageOCR = PageOCR(name)
    logging.debug('Loading PDF: {}'.format(filename))
    image_pdf = Image(filename=filename, resolution = 300)
    logging.debug('Converting PDF->JPEG')
    image_jpeg = image_pdf.convert('jpeg')
    image_pdf = None

    final_text = []
    counter = 0
    employee = None
    error_detected = False
    for page in image_jpeg.sequence:
        counter = counter + 1
        logging.debug('Processing page %d.', counter)
        img = PI.open(io.BytesIO(Image(image=page).make_blob('jpeg')))
        pageOCR.page = counter
        pageOCR.img = img

        for i in range(37): # Each page has a maximum of 37 lines.
            d = i * 51      # Each row has a height of 51 pixels.
            naf = pageOCR.scan((68, 896 + d, 280, 930 + d), text=False)
            if len(naf.strip()) > 0:
                if not employee is None:
                    if not error_detected:
                        logging.debug('Storing employee: %s', str(employee))
                        employees[employee.naf] = employee
                    else:
                        error_detected = False

                ipf = pageOCR.scan((288, 896 + d, 551, 930 + d))
                caf = pageOCR.scan((559, 896 + d, 714, 930 + d))
                try:
                    employee = Employee(naf, ipf, caf)
                except ValueError as e:
                    logging.error('Error doing OCR on page %d, line %d: %s.',
                                  counter, i + 1, e)
                    error_detected = True

                if not error_detected:
                    # Check consistency with previous data
                    if employee.naf in employees:
                        if employee != employees[employee.naf]:
                            logging.warn('Inconsistency with previous ' +
                                'data of Employee NAF = %s.', employee.naf)

                        employee = employees[employee.naf]
                        logging.debug('Recovering previous employee: %s',
                                      str(employee))

            mes = pageOCR.scan((722, 896 + d, 797, 930 + d), text=False)
            if len(mes.strip()) == 0:
                if not error_detected:
                    logging.debug('Storing employee: %s', str(employee))
                    employees[employee.naf] = employee
                else:
                    error_detected = False
                break; # If mes is empty the rest of the page is empty.

            if error_detected:
                logging.debug('Skipping line %d of page %d because an error',
                              i + 1, counter)
                continue;

            p = Payment()
            try:
                p.mes = mes
                p.indicador = pageOCR.scan((805, 896 + d, 960, 930 + d),
                                        tesseract_layout=10)    # Single char.
                p.dhcotizadas = pageOCR.scan((968, 896 + d, 1098, 930 + d))
                p.tipocto = pageOCR.scan((1106, 896 + d, 1162, 930 + d),
                                        text=False)             # Digit.
                p.ocup = pageOCR.scan((1170, 896 + d, 1232, 930 + d),
                                        tesseract_layout=10)    # Single char.
                p.descripcion = pageOCR.scan((1240, 896 + d, 1578, 930 + d))
                p.importe = pageOCR.scan((1586, 896 + d, 1734, 930 + d))
                p.dias = pageOCR.scan((1742, 896 + d, 1800, 930 + d),
                                        text=False)             # Digit.
                p.tresol = pageOCR.scan((1808, 896 + d, 1911, 930 + d))
                p.fresol = pageOCR.scan((1919, 896 + d, 2080, 930 + d))
                p.finicio = pageOCR.scan((2088, 896 + d, 2240, 930 + d))
                p.ffin = pageOCR.scan((2248, 896 + d, 2387, 930 + d))
                p.location = location
                p.year = year
                employee.addPayment(p)
            except ValueError as e:
                logging.error('Error doing OCR on page %d, line %d: %s.',
                              counter, i + 1, e)
                error_detected = True

    if not error_detected and not employee is None:
        logging.debug('Storing employee: %s', str(employee))
        employees[employee.naf] = employee

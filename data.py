"""
TC2 Extractor data objects.

CLASSES:
- Employee: stores the employee information and all his/her payments.
- Payment: stores the payment (contribution) information.

COPYRIGHT:
Copyright 2016 Javier Moreno López

This file is part of TC2Extractor.

TC2Extractor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TC2Extractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TC2Extractor.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import re

class Employee:
    """
    Stores the employee information and all his/her payments.
    """

    # Validation
    _naf_re = re.compile(r"^\d{2}\s\d{10}$")
    _ipf_re = re.compile(r"^(1\s\d{13}[A-Z]|(6|2)\s0{5}[A-Z]\d{7}[A-Z])$")
    _caf_re = re.compile(r"^[a-zñáéíóúã'\s]{3,5}$", re.IGNORECASE)

    # OCR Recovery
    _nie_subs = {'X': '0', 'Y': '1', 'Z': '2'} # NIE control digit substitution

    @property
    def naf(self):
        return self._naf

    @property
    def ipf(self):
        return self._ipf

    @property
    def caf(self):
        return self._caf

    @property
    def payments(self):
        return self._payments

    @naf.setter
    def naf(self, naf):
        txt = naf.replace(' ', '')
        txt = txt[:2] + ' ' + txt[2:]
        if len(txt) == 13 and not self._naf_re.match(txt) is None:
            self._naf = txt
            if txt != naf:
                logging.debug('NAF "%s" changed to "%s"', naf, txt)
        else:
            raise ValueError('Wrong NAF: ' + naf)

    @ipf.setter
    def ipf(self, ipf):
        txt = ipf.replace(' ', '')
        if len(txt) != 15:
            raise ValueError('Wrong IPF: ' + ipf)

        txt = txt[:1] + ' ' + txt[1:]
        if self._ipf_re.match(txt) is None:
            # Try to recover from OCR errors.
            # Sometimes digits are recognized as letters
            txt = txt[:3] + (txt[3:15].replace('B', '8')
                                      .replace('Q', '9')
                            ) + txt[-1:]

            if txt[-1].isdigit(): # Last position must be a letter.
                # Letter algorithm:
                # http://www.ordenacionjuego.es/es/calculo-digito-control
                number = None
                if txt[0] == '1' and txt[3:15].isdigit():
                    number = txt[3:15] # It's a DNI.
                elif (txt[0] == '6'
                        and txt[7] in self._nie_subs
                        and txt[8:15].isdigit()):
                    # It's a NIE.
                    number = self._nie_subs[txt[7]] + txt[8:15]

                if not number is None:
                    txt = (txt[:-1] +
                                    "TRWAGMYFPDXBNJZSQVHLCKE"[int(number)%23])

        if  not self._ipf_re.match(txt) is None:
            self._ipf = txt
            if txt != ipf:
                logging.debug('IPF "%s" changed to "%s"', ipf, txt)
        else:
            raise ValueError('Wrong IPF: ' + ipf)

    @caf.setter
    def caf(self, caf):
        txt = caf.replace(' ', '')
        if self._caf_re.match(txt) is None:
            raise ValueError('Wrong CAF: ' + caf)
        else:
            if txt != caf:
                logging.debug('CAF "%s" changed to "%s"', caf, txt)
            self._caf = txt

    def __init__(self, naf, ipf, caf):
        self._payments = []
        self.naf = naf
        self.ipf = ipf
        self.caf = caf

    def __eq__(self, employee):
        return (self.naf == employee.naf) and (self.ipf == employee.ipf) \
            and (self.caf.upper() == employee.caf.upper())

    def __str__(self):
        return "[Employee: naf = '" + self.naf + "', ipf = '" + self.ipf + \
               "', caf = '" + self.caf + \
               "', # of payments = " + str(len(self.payments)) + "]"

    def addPayment(self, payment):
        self.payments.append(payment)


class Payment:
    """
    Stores the payment information.

    Optional attributes:
     - indicador
     - ocup
     - dias
     - tresol
     - fresol
     - finicio
     - ffin
    """

    # Validation
    _indicador_re = re.compile(r"^[A-Z](\s[A-Z])*$")
    _ocup_re = re.compile(r"^[a-z]$")
    importe_re = re.compile(r"^([1-3]\.\d{3}|\d{1,3}),\d{2}?$")

    # OCR Recovery
    _importe_bad_separator_re = re.compile(r"^([1-3]\.\d{3}|\d{1,3}).\d{2}?$")
    _descripcion_re01 = re.compile(r"^Contin(.){1,2}\.\sComunes\.$")
    _descripcion_re02 = re.compile(r"^Base\sAT\s.\sEP\.$")
    _descripcion_re03 = re.compile(r"^Boni.{1,2}caciones.$")

    def isEmpty(value):
        return value is None or len(value) == 0

    @property
    def year(self):
        return self._year

    @property
    def mes(self):
        return self._mes

    @property
    def indicador(self):
        return self._indicador

    @property
    def dhcotizadas(self):
        return self._dhcotizadas

    @property
    def tipocto(self):
        return self._tipocto

    @property
    def ocup(self):
        return self._ocup

    @property
    def descripcion(self):
        return self._descripcion

    @property
    def importe(self):
        return self._importe

    @property
    def dias(self):
        return self._dias

    @property
    def tresol(self):
        return self._tresol

    @property
    def fresol(self):
        return self._fresol

    @property
    def finicio(self):
        return self._finicio

    @property
    def ffin(self):
        return self._ffin

    @property
    def location(self):
        return self._location

    @year.setter
    def year(self, year):
        if year.isdigit() and int(year) in range(1900,9999):
            self._year = int(year)
        else:
            raise ValueError('Wrong YEAR: ' + year)

    @mes.setter
    def mes(self, mes):
        if mes.isdigit() and int(mes) in range(1, 13):
            self._mes = int(mes)
        else:
            raise ValueError('Wrong MES: ' + mes)

    @indicador.setter
    def indicador(self, indicador):
        if (Payment.isEmpty(indicador) or
                not self._indicador_re.match(indicador) is None):
            self._indicador = indicador
        else:
            raise ValueError('Wrong INDICADOR: ' + indicador)

    @dhcotizadas.setter
    def dhcotizadas(self, dhcotizadas):
        txt = dhcotizadas.replace(' ', '').replace('O','0')
        if txt.isdigit():
            self._dhcotizadas = txt
            if txt != dhcotizadas:
                logging.debug('DHCotizadas "%s" changed to "%s"',
                                dhcotizadas, txt)
        elif ((txt[-1:] == 'D' \
                or txt[-1:] == 'H') \
                and txt[:-1].isdigit() \
                and int(txt[:-1]) in range(1, 32)):
            if txt != dhcotizadas:
                logging.debug('DHCotizadas "%s" changed to "%s"',
                                dhcotizadas, txt)
            self._dhcotizadas = txt[:-1] + ' ' + txt[-1:]
        else:
            raise ValueError('Wrong DHCOTIZADAS: ' + dhcotizadas)

    @tipocto.setter
    def tipocto(self, tipocto):
        if tipocto.isdigit():
            self._tipocto = tipocto
        else:
            raise ValueError('Wrong TIPOCTO: ' + tipocto)

    @ocup.setter
    def ocup(self, ocup):
        if Payment.isEmpty(ocup) or not self._ocup_re.match(ocup) is None:
            self._ocup = ocup
        else:
            raise ValueError('Wrong OCUP: ' + ocup)

    @descripcion.setter
    def descripcion(self, descripcion):
        if descripcion is None or len(descripcion.strip()) == 0:
            raise ValueError("Description can't be empty")

        txt = descripcion.strip()
        if self._descripcion_re01.match(txt) is not None:
            txt = 'Conting. Comunes.'
        elif self._descripcion_re02.match(txt) is not None:
            txt = 'Base AT y EP.'
        elif self._descripcion_re03.match(txt) is not None:
            txt = 'Bonificaciones.'

        if txt != descripcion:
            logging.debug('Descripcion "%s" changed to "%s"',
                            descripcion, txt)
        self._descripcion = txt

    @importe.setter
    def importe(self, importe):
        txt = importe.replace(' ', '')
        if not self.importe_re.match(txt) is None:
            temp = float(txt.replace('.', '').replace(',', '.'))
            if temp < 0:
                raise ValueError('Wrong IMPORTE: ' + importe)

            if txt != importe:
                logging.debug('Importe "%s" changed to "%f"', importe, temp)
            self._importe = temp
        else:
            # Recovering from OCR errors.
            if not self._importe_bad_separator_re.match(txt) is None:
                # Sometimes the decimal separator is detected as . instead of ,
                temp1 = txt.replace('.', '')
                temp = float(temp1[:-2] + '.' + temp1[-2:])
                if txt != importe:
                    logging.debug('Importe "%s" changed to "%f"',
                                    importe, temp)
                self._importe = temp
            else:
                raise ValueError('Wrong IMPORTE: ' + importe)

    @dias.setter
    def dias(self, dias):
        if not Payment.isEmpty(dias):
            if dias.isdigit() and int(dias) in range(1, 32):
                self._dias = int(dias)
            else:
                raise ValueError('Wrong DIAS: ' + dias)

    @tresol.setter
    def tresol(self, tresol):
        self._tresol = tresol

    @fresol.setter
    def fresol(self, fresol):
        self._fresol = fresol

    @finicio.setter
    def finicio(self, finicio):
        self._finicio = finicio

    @ffin.setter
    def ffin(self, ffin):
        self._ffin = ffin

    @location.setter
    def location(self, location):
        if len(location) == 3:
            self._location = location.upper()
        else:
            raise ValueError('Wrong LOCATION:' + location)

    def __init__(self):
        self._indicador = None
        self._ocup = None
        self._dias = None
        self._tresol = None
        self._fresol = None
        self._finicio = None
        self._ffin = None

    def __str__(self):
        result = []
        result.append("year = " + str(self.year))
        result.append("mes = " + str(self.mes))
        if not self.indicador is None:
            result.append("indicador = '" + self.indicador + "'")
        result.append("dhcotizadas = '" + self.dhcotizadas + "'")
        result.append("tipocto = '" + self.tipocto + "'")
        if not self.ocup is None:
            result.append("ocup = '" + self.ocup + "'")
        result.append("descripcion = '" + self.descripcion + "'")
        result.append("importe = " + str(self.importe))
        if not self.dias is None:
            result.append("dias = '" + str(self.dias) + "'")
        if not self.tresol is None:
            result.append("tresol = '" + self.tresol + "'")
        if not self.fresol is None:
            result.append("fresol = '" + self.fresol + "'")
        if not self.finicio is None:
            result.append("finicio = '" + self.finicio + "'")
        if not self.ffin is None:
            result.append("ffin = '" + self.ffin + "'")
        result.append("location = '" + self.location + "'")

        return '[Payment: ' + ', '.join(result) + ']'

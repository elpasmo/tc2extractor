"""
TC2 Extractor text import module.

COPYRIGHT:
Copyright 2016 Javier Moreno López

This file is part of TC2Extractor.

TC2Extractor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TC2Extractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TC2Extractor.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import re

from data import Employee, Payment

def processpayment(employee, line, location, year, line_number):
    logging.debug('processpayment(%s, %s, %s, %s, %d)', employee.naf,
                  line, location, year, line_number)
    payment = Payment()
    payment.mes = line[:2]
    desplazamiento = 0

    #Indicador
    while not line[3 + desplazamiento:4 + desplazamiento].isdigit():
        desplazamiento += 2
    if desplazamiento > 0:
        logging.debug('Found indicador: %s', line[3:4 + desplazamiento - 2])
        payment.indicador = line[3:4 + desplazamiento - 2]

    #D/H cotizadas
    words = line[3 + desplazamiento:].split()
    desplazamiento += len(words[0])
    if words[1] == 'D' or words[1] == 'H':
        logging.debug('Found dhcotizadas with indicator')
        payment.dhcotizadas = words[0] + ' ' + words[1]
        desplazamiento += 2
    else:
        logging.debug('Found dhcotizadas in hours')
        payment.dhcotizadas = words[0]

    payment.tipocto = line[4 + desplazamiento:7 + desplazamiento]

    #Ocup
    if ' ' == line[9 + desplazamiento:10 + desplazamiento]:
        logging.debug('Found ocup: %s', \
                      line[8 + desplazamiento:9 + desplazamiento])
        payment.ocup =line[8 + desplazamiento:9 + desplazamiento]
        desplazamiento += 2

    words = line[8 + desplazamiento:].split()

    #Searching importe
    num_words = len(words) - 1
    position = num_words
    found = False
    logging.debug('Searching importe in: %s',line[8 + desplazamiento:])
    while position >= 0 and not found:
        if payment.importe_re.match(words[position]) is None:
            position -= 1
        else:
            logging.debug('Found importe: %s', words[position])
            found = True

    if found:
        payment.importe = words[position]
        payment.descripcion = ' '.join(words[:position])
        position += 1
        if position <= num_words:
            logging.debug('Found dias: %s', words[position])
            payment.dias = words[position]
            position += 1
            if position <= num_words:
                logging.debug('Found tresol: %s', words[position])
                payment.tresol = words[position]
                position += 1
                if position <= num_words:
                    logging.debug('Found fresol: %s', words[position])
                    payment.fresol = words[position]
                    position += 1
                    if position <= num_words:
                        logging.debug('Found finicio: %s', words[position])
                        payment.finicio = words[position]
                        position += 1
                        if position == num_words:
                            logging.debug('Found ffin: %s', words[position])
                            payment.ffin = words[position]
                        else:
                            raise ValueError('{} for employee {}: "{}"'.format(
                                    'Too much data', employee, line))
    else:
        raise ValueError('{} for employee {}: "{}"'.format(
                    'Importe not found', employee, line))

    payment.location = location
    payment.year = year
    employee.addPayment(payment)
    logging.debug('Added %s to employee NAF: %s', str(payment), employee.naf)

def importTC2s(employees, input_file, location, year):
    logging.debug('Extracting data for location %s and year %s',
                  location, year)

    error_detected = False
    employee = None
    line_number = 0
    for line in input_file:
        logging.debug('Processing line: %s', line.strip('\n'))
        line_number += 1
        line = line.strip().strip('\n')
        if line[0:2].isdigit() and len(line) > 2:
            if line[2:12].isdigit() and line[12] == ' ':
                # OCR may not detect the space at position 2
                logging.warning('Adding space to NAF. Old="%s", new="%s"',
                                line[:12], line[:2] + ' ' + line[2:12])
                line = line[:2] + ' ' + line[2:]

            if line[3:13].isdigit() and line[13] == ' ': #NAF
                if not employee is None:
                    if not error_detected:
                        logging.debug('Storing employee: %s', str(employee))
                        employees[employee.naf] = employee

                    error_detected = False

                try:
                    if line[29] == ' ':
                        # NAF is assumed to have a right value. If position 29
                        # is a space then it's assumed that the letter of the
                        # IPF is wrongly separated.
                        logging.warning('Removing space from IPF. Old="%s", ' +
                                        'new="%s"', line[14:31],
                                        line[14:29]+ line[30])
                        line = line[:29] + line[30:]
                    employee = Employee(line[0:13], line[14:30], line[31:36])
                except ValueError as e:
                    logging.exception("ValueError processing: {}.".format(line))
                    error_detected = True

                if not error_detected:
                    # Check consistency with previous data
                    if employee.naf in employees:
                        if employee != employees[employee.naf]:
                            logging.warn('Inconsistency with previous ' +
                                'data of Employee NAF = {}'.format(
                                    employee.naf) + ' ' + line)

                        employee = employees[employee.naf]
                        logging.debug('Recovering previous employee: %s',
                                      str(employee))
                    try:
                        if line[35] == ' ':
                            logging.debug('Employee processed: %s Processing ' +
                                        'payment from position 36',
                                        str(employee))
                            processpayment(employee, line[36:],
                                           location, year, line_number)
                        else:
                            logging.debug('Employee processed: %s Processing ' +
                                        'payment from position 37',
                                        str(employee))
                            processpayment(employee, line[37:],
                                           location, year, line_number)
                    except ValueError as e:
                        logging.exception("ValueError processing: {}.".
                            format(line))
            elif not error_detected: #Payment
                try:
                    processpayment(employee, line, location, year, line_number)
                except ValueError as e:
                    logging.exception("ValueError processing: {}.".format(line))
            else:
                logging.debug('Skipping line because of an existing error')
        else:
            logging.debug('Skipping line because there is no data of interest')

    if not error_detected and not employee is None:
        logging.debug('Storing employee: %s', str(employee))
        employees[employee.naf] = employee

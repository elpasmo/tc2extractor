"""
TC2 Extractor SQLite export module.

FUNCTIONS:
- check_schema: check if the sqlite database schema is valid.
- create_schema: creates the sqlite database schema with two tables.
- files_processed: returns a list of filenames that've been already processed.
- table_exists: check if a table exists.
- storeTC2s: stores all the TC2's information in the database.

COPYRIGHT:
Copyright 2016 Javier Moreno López

This file is part of TC2Extractor.

TC2Extractor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TC2Extractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TC2Extractor.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import sqlite3

def create_schema(cursor):
    """
    Creates the SQLite database schema.

    Its only parameter is the cursor to the target database file.

    The tables are:
     - 'pagos': stores Payment information.
     - 'personas': stores Employee information.
     - 'ficheros': stores the filenames that already has been processed.
    """
    logging.info('Creating tables.')
    cursor.execute('''CREATE TABLE  personas(   id INTEGER PRIMARY KEY,
                                                naf TEXT(13) unique,
                                                ipf TEXT(16) unique,
                                                caf TEXT(5))''')
    cursor.execute('''CREATE TABLE  pagos(  id INTEGER PRIMARY KEY,
                                            personas_id INTEGER,
                                            year INTEGER,
                                            mes INTEGER,
                                            indicador TEXT,
                                            dhcotizadas TEXT(4),
                                            tipocto TEXT(3),
                                            ocup TEXT(1),
                                            descripcion TEXT,
                                            importe REAL,
                                            dias INTEGER,
                                            tresol TEXT,
                                            fresol TEXT,
                                            finicio TEXT,
                                            ffin TEXT,
                                            location TEXT(3),
                                            FOREIGN KEY (personas_id)
                                            REFERENCES personas(id))''')
    cursor.execute('''CREATE TABLE  ficheros(   id INTEGER PRIMARY KEY,
                                                nombre TEXT(16) unique)''')

def table_exists(cursor, name):
        cursor.execute("""SELECT name FROM sqlite_master
                          WHERE name = '{}' and type='table'""".format(name))

        exists = not cursor.fetchone() is None
        logging.debug('Is table "%s" present? %s', name, str(exists))
        return exists

def check_schema(path):
    """
    Check if the SQLite database schema is correct.

    Its only parameter is the path to the target database file.

    If the database is empty or the required tables don't exist it creates them.
    """
    logging.debug('Checking database schema: {}.'.format(path))
    db = sqlite3.connect(path)
    try:
        cursor = db.cursor()
        personas_exist = table_exists(cursor, 'personas')
        pagos_exist = table_exists(cursor, 'pagos')
        ficheros_exist = table_exists(cursor, 'ficheros')

        if not(personas_exist or pagos_exist or ficheros_exist):
            try:
                create_schema(cursor)
                db.commit()
            except Exception as e:
                db.rollback()
                raise e
        elif not(personas_exist and pagos_exist and ficheros_exist):
            names = []
            if ficheros_exist:
                names.append('ficheros')
            if pagos_exist:
                names.append('pagos')
            if personas_exist:
                names.append('personas')
            logging.fatal('Database is corrupted or not valid. ' +
                          'The following tables ahora missing: {}.'.format(
                                                            ', '.join(names)))
            return False
        else:
            # Check 'ficheros' columns
            id_ok, nombre_ok = False, False
            for row in cursor.execute('PRAGMA table_info("ficheros")'):
                if row[1] == 'id':
                    # Check type and if it's PK
                    id_ok = (row[2] == 'INTEGER' and row[5] == 1)
                elif row[1] == 'nombre':
                    nombre_ok = (row[2] == 'TEXT(16)')

            if False in (id_ok, nombre_ok):
                logging.fatal('Table "ficheros" has not required fields: ' +
                        'id={}, nombre={}'.format(id_ok, nombre_ok))
                return False

            # Check 'personas' columns
            id_ok, naf_ok, ipf_ok, caf_ok = False, False, False, False
            for row in cursor.execute('PRAGMA table_info("personas")'):
                if row[1] == 'id':
                    # Check type and if it's PK
                    id_ok = (row[2] == 'INTEGER' and row[5] == 1)
                elif row[1] == 'naf':
                    naf_ok = (row[2] == 'TEXT(13)')
                elif row[1] == 'ipf':
                    ipf_ok = (row[2] == 'TEXT(16)')
                elif row[1] == 'caf':
                    caf_ok = (row[2] == 'TEXT(5)')

            if False in (id_ok, naf_ok, ipf_ok, caf_ok):
                logging.fatal('Table "personas" has not required fields: ' +
                        'id={}, naf={}, ipf={}, caf={}'.format(id_ok, naf_ok,
                                                            ipf_ok, caf_ok))
                return False

            # Check 'pagos' columns
            id_ok, personas_id_ok, year_ok, mes_ok, indicador_ok, \
                    dhcotizadas_ok, tipocto_ok, ocup_ok, descripcion_ok, \
                    importe_ok, dias_ok, tresol_ok, fresol_ok, finicio_ok, \
                    ffin_ok, location_ok = False, False, False, False, False, \
                                           False, False, False, False, False, \
                                           False, False, False, False, False, \
                                           False
            for row in cursor.execute('PRAGMA table_info("pagos")'):
                if row[1] == 'id':
                    # Check type and if it's PK
                    id_ok = (row[2] == 'INTEGER' and row[5] == 1)
                elif row[1] == 'personas_id':
                    personas_id_ok = (row[2] == 'INTEGER')
                elif row[1] == 'year':
                    year_ok = (row[2] == 'INTEGER')
                elif row[1] == 'mes':
                    mes_ok = (row[2] == 'INTEGER')
                elif row[1] == 'indicador':
                    indicador_ok = (row[2] == 'TEXT')
                elif row[1] == 'dhcotizadas':
                    dhcotizadas_ok = (row[2] == 'TEXT(4)')
                elif row[1] == 'tipocto':
                    tipocto_ok = (row[2] == 'TEXT(3)')
                elif row[1] == 'ocup':
                    ocup_ok = (row[2] == 'TEXT(1)')
                elif row[1] == 'descripcion':
                    descripcion_ok = (row[2] == 'TEXT')
                elif row[1] == 'importe':
                    importe_ok = (row[2] == 'REAL')
                elif row[1] == 'dias':
                    dias_ok = (row[2] == 'INTEGER')
                elif row[1] == 'tresol':
                    tresol_ok = (row[2] == 'TEXT')
                elif row[1] == 'fresol':
                    fresol_ok = (row[2] == 'TEXT')
                elif row[1] == 'finicio':
                    finicio_ok = (row[2] == 'TEXT')
                elif row[1] == 'ffin':
                    ffin_ok = (row[2] == 'TEXT')
                elif row[1] == 'location':
                    location_ok = (row[2] == 'TEXT(3)')

            if False in (id_ok, personas_id_ok, year_ok, mes_ok, \
                         indicador_ok, dhcotizadas_ok, tipocto_ok, ocup_ok, \
                         descripcion_ok, importe_ok, dias_ok, tresol_ok, \
                         fresol_ok, finicio_ok, ffin_ok, location_ok):
                logging.fatal('Table "pagos" has not required fields: ' +
                              'id={}, '.format(id_ok) +
                              'personas_id={}, '.format(id_ok) +
                              'year={}, '.format(year_ok) +
                              'mes={}, '.format(mes_ok) +
                              'indicador={}, '.format(indicador_ok) +
                              'dhcotizadas={},  '.format(dhcotizadas_ok) +
                              'tipocto={},  '.format(tipocto_ok) +
                              'ocup={}, '.format(ocup_ok) +
                              'descripcion={}, '.format(descripcion_ok) +
                              'importe={}, '.format(importe_ok) +
                              'dias={}, '.format(dias_ok) +
                              'tresol={}, '.format(tresol_ok) +
                              'fresol={}. '.format(fresol_ok) +
                              'finicio={}, '.format(finicio_ok) +
                              'ffin={}, '.format(ffin_ok) +
                              'location={}'.format(location_ok))
                return False
    finally:
        db.close()

    logging.debug('Database OK.')
    return True

def files_processed(path):
    """
    Returns a list of filenames already processed.

    Parameters:
     - path: target database file path.
    """
    db = sqlite3.connect(path)
    try:
        cursor = db.cursor()
        filenames = []
        for row in cursor.execute('SELECT nombre FROM ficheros'):
            filenames.append(row[0])
        if len(filenames) > 0:
            logging.debug('Files already processed: %s.', ', '.join(filenames))
        return filenames
    except Exception as e:
        raise e
    finally:
        db.close()

def storeTC2s(path, employees, filenames):
    """
    Stores all the TC2's information in the database.

    Parameters:
     - path: target database file path.
     - employees: list of Employee information.
     - filenames: list of filenames that were processed for these employees.
    """

    db = sqlite3.connect(path)
    try:
        cursor = db.cursor()
        for k, v in employees.items():
            logging.debug('Inserting employee: %s', str(v))
            last_id = None
            try:
                cursor.execute("""INSERT INTO personas(naf, ipf, caf)
                                              VALUES(?, ?, ?)""",
                               (v.naf, v.ipf, v.caf))
                last_id = cursor.lastrowid
            except sqlite3.IntegrityError as e:
                logging.debug('IntegrityError: checking if the employee ' +
                              'already exists in the database.')
                cursor.execute("""SELECT naf, caf, id FROM personas
                                              WHERE ipf = :ipf""",
                                              {'ipf': v.ipf})
                result = cursor.fetchone()
                if result is None:
                    logging.debug('Check failed.')
                    raise e
                elif result[0] != v.naf or result[1] != v.caf:
                    logging.warning('Found mismatch information in the ' +
                                    'database. Expected naf="%s" and caf="%s" '+
                                    'for ipf="%s" but found naf="%s" and ' +
                                    'caf="%s" instead.', v.naf, v.caf, v.ipf,
                                    result[0], result[1])
                else:
                    logging.debug('Found existing employee.')
                last_id = result[2]

            for p in v.payments:
                query = """INSERT INTO pagos(personas_id, year, mes,
                                                 dhcotizadas, tipocto,
                                                 descripcion, importe, location
                                                 """
                values_query = ') VALUES(?, ?, ?, ?, ?, ?, ?, ?'
                values = [last_id, p.year, p.mes, p.dhcotizadas, p.tipocto,
                          p.descripcion, p.importe, p.location]
                if not p.indicador is None:
                    query += ', indicador'
                    values_query += ', ?'
                    values.append(p.indicador)
                if not p.ocup is None:
                    query += ', ocup'
                    values_query += ', ?'
                    values.append(p.ocup)
                if not p.dias is None:
                    query += ', dias'
                    values_query += ', ?'
                    values.append(p.dias)
                if not p.tresol is None:
                    query += ', tresol'
                    values_query += ', ?'
                    values.append(p.tresol)
                if not p.fresol is None:
                    query += ', fresol'
                    values_query += ', ?'
                    values.append(p.fresol)
                if not p.finicio is None:
                    query += ', finicio'
                    values_query += ', ?'
                    values.append(p.finicio)
                if not p.ffin is None:
                    query += ', ffin'
                    values_query += ', ?'
                    values.append(p.ffin)

                logging.debug('Inserted payment: %s for employee id: %d',
                        str(p), last_id)
                cursor.execute(query + values_query + ')', tuple(values))

            for f in filenames:
                logging.debug('Inserting processed file: %s.', f)
                # Failing normal inserts at empty table. Reason: Integrity
                # error because of unique constraint. Added "OR IGNORE".
                cursor.execute("""INSERT OR IGNORE INTO ficheros(nombre)
                                                VALUES(?)""", (f,))

        db.commit()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()

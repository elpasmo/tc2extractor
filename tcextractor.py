"""
TC2 Extractor main file.

COPYRIGHT:
Copyright 2016 Javier Moreno López

This file is part of TC2Extractor.

TC2Extractor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TC2Extractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TC2Extractor.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import logging

from import_pdf import importTC2s as import_pdf
from import_txt import importTC2s as import_txt
from export_sqlite import check_schema, storeTC2s, files_processed

database = 'tc2.db'
logging.basicConfig(filename='tcimport.log',
                    format='[%(levelname)s]%(asctime)s%(message)s',
                    datefmt='[%y%m%d][%H:%M:%S]',
                    level=logging.DEBUG)
logging.info('Initiating tc2 extraction to database: {}.'.format(database))
if check_schema(database):
    files_already_processed = files_processed(database)
    employees = {}
    processed_files = []
    pdf_ocr_present = True
    for f in sorted(os.listdir()):
        if not os.path.isdir(f):
            if ('txt' == f.split('.')[len(f.split('.'))-1].lower()
                    and len(f) > 12):
                if f in files_already_processed:
                    logging.info('Skipping: %s', f)
                else:
                    logging.info('Processing: %s', f)
                    with open(f, 'r', encoding='utf-8') as input_file:
                        import_txt(employees, input_file, f[:3], f[3:7])
                    processed_files.append(f)
            elif 'pdf' == f.split('.')[len(f.split('.'))-1].lower():
                if f in files_already_processed:
                    logging.info('Skipping: %s', f)
                elif pdf_ocr_present:
                    logging.info('Processing: %s', f)
                    try:
                        import_pdf(employees, f)
                        processed_files.append(f)
                    except ImportError as e:
                        pdf_ocr_present = False
                        logging.critical(e)

    storeTC2s(database, employees, processed_files)
logging.info('Finishing tc2 extraction to database: {}.'.format(database))
